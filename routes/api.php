<?php
//
namespace Illuminate\Support\Facades\Route;
use App\Http\Controllers\SaveGameController;
use App\Http\Controllers\GetGameController;


use Illuminate\Support\Facades\Route;

Route::get('/test', [GetGameController::class, 'backToGame']);

Route::post('/test/game',[SaveGameController::class, 'createGame']);
