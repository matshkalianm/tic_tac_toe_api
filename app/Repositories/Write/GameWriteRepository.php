<?php


namespace App\Repositories\Write;


use App\Models\savingGame;


class GameWriteRepository implements GameWriteRepositoryInterface
{
    public function createGame($game, $turn)
    {
        $newGame = savingGame::create
        (
            json_encode($game),
            $turn
        );
        return $newGame;
    }

}
