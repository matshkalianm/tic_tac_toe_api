<?php


namespace App\Repositories\Write;


interface GameWriteRepositoryInterface
{
    public function createGame
    (
        $game,
        $turn
    );
}
