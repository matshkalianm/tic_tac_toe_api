<?php


namespace App\Repositories\Read;


use App\Models\savingGame;

class GameReadRepository implements GameReadRepositoryInterface
{
    public function getById()
    {
        $lastGame = savingGame::find(savingGame::max('id'));
        return $lastGame;
    }
}
