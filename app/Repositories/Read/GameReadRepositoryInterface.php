<?php


namespace App\Repositories\Read;


interface GameReadRepositoryInterface
{
    public function getById();
}
