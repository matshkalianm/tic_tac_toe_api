<?php

namespace App\Http\Controllers;
use App\Http\Requests\GameRequest;
use App\Service\Dto\CreateGameDto;
use App\Service\GameService;
use Illuminate\Http\Request;

class SaveGameController extends Controller
{
    protected GameService $gameService;

    public function __construct( GameService $gameService )
    {
        $this->gameService = $gameService;
    }

    public function createGame(GameRequest $gameRequest)
    {
        $createGameDto = new CreateGameDto(
            $gameRequest->setGame(),
            $gameRequest->setTurn()
        );
        $this->gameService->create($createGameDto);
    }
}
