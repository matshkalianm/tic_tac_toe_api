<?php

namespace App\Http\Controllers;

use App\Service\GameService;

class GetGameController extends Controller
{
    protected GameService $gameService;

    public function __construct( GameService $gameService ){
           $this->gameService = $gameService;
    }

    public function backToGame()
    {
       $lastGame=$this->gameService->getGame();
       return json_encode($lastGame);
    }
}
