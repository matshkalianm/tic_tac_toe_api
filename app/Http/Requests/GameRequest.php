<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class GameRequest extends FormRequest
{
    const GAME = 'game';
    const TURN = 'turn';

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [

        ];
    }
    public function setGame(){
        return $this->get(self::GAME);
    }

    public function setTurn(){
        return $this->get(self::TURN);
    }

}
