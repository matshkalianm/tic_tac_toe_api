<?php
//
namespace App\Models;
//
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
//
class savingGame extends Model

{
    use HasFactory;

    public function setGame(string $game){
        $this->game=$game;
    }

    public function setTurn(string $turn){
        $this->turn=$turn;
    }


    public static function create($game, $turn)
    {
        $entity = new self();
        $entity->setGame($game);
        $entity->setTurn($turn);

        return $entity;

    }
}
