<?php


namespace App\Service;


use App\Repositories\Read\GameReadRepository;
use App\Repositories\Write\GameWriteRepository;
use App\Service\Dto\CreateGameDto;
use App\Service\Dto\GetGameDto;

class GameService
{
    protected GameWriteRepository $gameWriteRepository;
    protected GameReadRepository $gameReadRepository;


    public function __construct(
        GameWriteRepository $gameWriteRepository,
        GameReadRepository $gameReadRepository
    )
    {
        $this->gameWriteRepository = $gameWriteRepository;
        $this->gameReadRepository = $gameReadRepository;

    }

    public function create(CreateGameDto $createGameDto)
    {
        $game = $this->gameWriteRepository->createGame(
            $createGameDto->game,
            $createGameDto->turn,
        );
        $game->save();
    }

    public function getGame()
    {
        $game = $this->gameReadRepository->getById();

        $restoreGame= Array(
            "game"=>$game->game,
            "turn"=>$game->turn
        );

        return $restoreGame;
    }
}
