<?php


namespace App\Service\Dto;


class CreateGameDto
{
    public  $game;
    public  $turn;

    public function __construct
    (
        $game,
        $turn
    )
    {
        $this->game = $game;
        $this->turn = $turn;
    }
}





